//
// sharing.cpp
//
// Copyright � 2012 jones@cs.tcd.ie
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// 19/11/12 first version
// 19/11/12 works with Win32 and x64
// 21/11/12 works with Character Set: "Not Set", "Use Unicode Chararcter Set" or "Use Multi-Byte Character Set"
// 21/11/12 output results so they can be easily pasted into a spreadsheet from console
//

//
// NB: include "windows.h" in sdtafx.h [quicker since it will be included in the pre-compiled headers]
//

#include "stdafx.h"                             //
#include "time.h"                               // time
#include "conio.h"                              // _getch
#include "intrin.h"                             // intrinsics
#include <iostream>                             // cout
#include <iomanip>                              // setprecision

using namespace std;                            // cout

#define K           1024                        //
#define GB          (K*K*K)                     //
#define NOPS        10000                       //
#define NSECONDS    2                           // run each test for NSECONDS

#define GINDX(n)    (g+n*lineSz/sizeof(long))   //

#define INTERLOCKED 0                           // 0:inc 1:InterlockedExchangeAdd

#if INTERLOCKED == 0
#define INC(g)      (*g)++;
#else
#define INC(g)      InterlockedExchangeAdd(g, 1)
#endif

clock_t tstart;                                 // start of test in ms

int ncpus;                                      // # cores
int sharing;                                    // % sharing
int lineSz;                                     // cache line size
int maxThread;                                  // max # of threads
int indx;                                       // results index

HANDLE *threadH;                                // thread handles
long long *cnt;                                 // for computing results
long long *r;                                   // results for pasting in spreadsheet from console
long long *diff;                                // results for pasting in spreadsheet from console

volatile long *g;                               // NB: position of volatile

//
// getCPUInfo
//
string getCPUInfo()
{
    string cpuInfo = "unrecognised CPU";

    MEMORYSTATUS ms;
    GlobalMemoryStatus(&ms);

    //
    // use registry to get processor type
    //
    HKEY hKey;
    DWORD len;
    char identifier[256] = "???";
    char processorNameString[256] = "???";

    if (RegOpenKeyExA(HKEY_LOCAL_MACHINE, "Hardware\\Description\\System\\CentralProcessor\\0", 0, KEY_READ, &hKey) == ERROR_SUCCESS) {

        len = sizeof(identifier);
        RegQueryValueExA(hKey, "Identifier", NULL, NULL, (BYTE*) &identifier, &len);
        cpuInfo = identifier;
        cpuInfo += "; ";
        len = sizeof(identifier);
        RegQueryValueExA(hKey, "ProcessorNameString", NULL, NULL, (BYTE*) &identifier, &len);
        cpuInfo += identifier;
        RegCloseKey(hKey);

    }

    //
    // remove extra space
    //
    size_t pos;
    while ((pos = cpuInfo.find("  ", 0)) != cpuInfo.npos)
        cpuInfo.erase(pos, 1);
    return cpuInfo;

}

//
// for CPU cache data returned by cpuid instruction
//
struct CpuIdData {
    int eax;
    int ebx;
    int ecx;
    int edx;
} cd;

//
// look for L1 cache line size (see Intel Application note on CPUID instruction)
//
int lookForL1DataCacheInfo(int v)
{
    if (v & 0x80000000)
        return 0;

    int sz = 0;

    for (int i = 0; i < 4; i++) {
        switch (v & 0xff) {
        case 0x0a:
        case 0x0c:
        case 0x10:
            return 32;
        case 0x0e:
        case 0x2c:
        case 0x60:
        case 0x66:
        case 0x67:
        case 0x68:
            return 64;
        }
        v >>= 8;
    }
    return 0;
}

//
// getL1DataCacheInfo
//
int getL1DataCacheInfo()
{
    __cpuid((int*) &cd, 2);

    if ((cd.eax & 0xff) != 1) {
        cout << "unrecognised cache type: default L= 64" << endl;
        return 64;
    }

    int sz;

    if (sz = lookForL1DataCacheInfo(cd.eax & ~0xff))
        return sz;
    if (sz = lookForL1DataCacheInfo(cd.ebx))
        return sz;
    if (sz = lookForL1DataCacheInfo(cd.ecx))
        return sz;
    if (sz = lookForL1DataCacheInfo(cd.edx))
        return sz;

    cout << "unrecognised cache type: default L= 64" << endl;
    return 64;
}

//
// getDeterministicCacheInfo
//
int getDeterministicCacheInfo()
{
    int type, cores, shared, ways, partitions, lineSz, sets;
    int i = 0;
    while (1) {
        __cpuidex((int*) &cd, 4, i);
        type = cd.eax & 0x1f;
        if (type == 0)
            break;
        cores = ((cd.eax >> 26) & 0x3f) + 1;
        shared = ((cd.eax >> 14) & 0x0fff) + 1;
        //cout << "cores=" << cores << " shared=" << shared << endl;
        cout << "L" << ((cd.eax >> 5) & 0x07);
        cout << ((type == 1) ? " D" : (type == 2) ? " I" : " U");
        ways = ((cd.ebx >> 22) & 0x03ff) + 1;
        partitions = ((cd.ebx) >> 12 & 0x03ff) + 1;
        sets = cd.ecx + 1;
        lineSz = (cd.ebx & 0x0fff) + 1;
        cout << " " << setw(5) << ways*partitions*lineSz*sets/1024 << "K" << " L=" << setw(3) << lineSz << " K=" << setw(3) << ways << " N=" << setw(5) << sets;
        cout << endl;
        i++;
    }
    return lineSz;
}

//
// getCacheLineSz
//
int getCacheLineSz()
{
    __cpuid((int*) &cd, 0);
    if (cd.eax >= 4)
        return getDeterministicCacheInfo();
    return getL1DataCacheInfo();
}

//
// worker
//
DWORD WINAPI worker(LPVOID vthread)
{
    int thread = (int) vthread;

    long long ops = 0;

    volatile long *gt = GINDX(thread);
    volatile long *gs = GINDX(maxThread);

    while (1) {

        //
        // do some work
        //
        for (int i = 0; i < NOPS / 4; i++) {

            switch (sharing) {
            case 0:

                INC(gt);
                INC(gt);
                INC(gt);
                INC(gt);
                break;

            case 25:
                INC(gt);
                INC(gt);
                INC(gt);
                INC(gs);
                break;

            case 50:
                INC(gt);
                INC(gs);
                INC(gt);
                INC(gs);
                break;

            case 75:
                INC(gt);
                INC(gs);
                INC(gs);
                INC(gs);
                break;

            case 100:
                INC(gs);
                INC(gs);
                INC(gs);
                INC(gs);

            }
        }
        ops += NOPS;

        //
        // check if runtime exceeded
        //
        if (clock() - tstart > NSECONDS*CLOCKS_PER_SEC)
            break;

    }

    cnt[thread] = ops;
    return 0;

}


//
// main
//
int _tmain(int argc, TCHAR* argv[])
{
    //cout << "sizeof(long) = " << sizeof(long) <<  " sizeof(int) = " << sizeof(int) << endl;

    //
    // get computer name
    //
    char name[256];
    DWORD sz = sizeof(name);
    GetComputerNameA(name, &sz);

    //
    // get number of CPUs
    //
    SYSTEM_INFO si;
    GetSystemInfo(&si);
    ncpus = si.dwNumberOfProcessors;
    maxThread = 2 * ncpus;

    //
    // get physical memorysize (RAM)
    //
    ULONGLONG ramSz;                            //
    GetPhysicallyInstalledSystemMemory(&ramSz); // NB: returns KB
    ramSz *= 1024;                              // now bytes

    //
    // get date
    //
    time_t t = time(NULL);
    struct tm now;
    char date[256];
    localtime_s(&now, &t);
    strftime(date, sizeof(date), "%d-%b-%Y", &now);

    //
    // console output
    //
    cout << name << ((sizeof(size_t) == 8) ? " x64 " : " Win32 ");
#if INTERLOCKED == 0
    cout << "inc";
#else
    cout << "InterlockedExchangeAdd";
#endif
    cout << " NCPUS=" << ncpus << " RAM=" << ramSz / GB << "GB NOPS=" << NOPS << " " << date << endl;
    cout << getCPUInfo().c_str() << endl;

    //
    // get L1 cache info
    //
    // ONLY works with later processors
    //
    lineSz = getCacheLineSz();
    //lineSz *= 2;

    //
    // allocate global variable
    //
    // NB: each element in g is stored in a different cache line to stop false sharing
    //
    threadH = (HANDLE*) _aligned_malloc(maxThread*sizeof(HANDLE), lineSz);      // thread handles
    cnt = (long long*) _aligned_malloc(maxThread*sizeof(long long), lineSz);    // for computing ops/s
    r = (long long*) _aligned_malloc(5*maxThread*sizeof(long long), lineSz);    // for results
    diff = (long long*) _aligned_malloc(5*maxThread*sizeof(long long), lineSz); // for results
    g = (long*) _aligned_malloc((maxThread+1)*lineSz, lineSz);                  // shared global variable
    indx = 0;

    for (sharing = 0; sharing <= 100; sharing += 25) {

        long long opspersec1 = 1;

        for (int nt = 1; nt <= 2*ncpus; nt *= 2) {

            //
            //  zero counters
            //
            for (int thread = 0; thread < nt; thread++)
                *(GINDX(thread)) = 0;
            *(GINDX(maxThread)) = 0;

            //
            // get start time
            //
            tstart = clock();

            //
            // create worker threads
            //
            for (int thread = 0; thread < nt; thread++)
                threadH[thread] = CreateThread(NULL, 0, worker, (LPVOID) thread, 0, NULL);

            //
            // wait for ALL worker threads to finish
            //
            WaitForMultipleObjects(nt, threadH, true, INFINITE);
            int runtime = clock() - tstart;

            //
            // output results summary on console
            //
            long long total = 0, incs = 0;
            for (int thread = 0; thread < nt; thread++) {
                total += cnt[thread];
                incs += *(GINDX(thread));
            }
            incs += *(GINDX(maxThread));
            long long opspersec = 1000 * total / runtime;
            if (nt == 1)
                opspersec1 = opspersec;
            r[indx] = opspersec;
            diff[indx++] = total-incs;
            double drt = double(runtime) / 1000.0;
            cout << "sharing=" << setw(3) << sharing << "% threads=" << setw(2) << nt;
            cout << " rt=" << setw(5) << fixed << setprecision(2) << drt;
            cout << " ops/s=" << setw(11) << opspersec << " relative=" << fixed << setprecision(2) << (double) opspersec / opspersec1;
            if (total == incs) {
                cout << " OK" << endl;
            } else {
                cout << " ERROR nops=" << total << " incs=" << incs << " [diff=" << total-incs << "]" << endl;
            }
            //
            // delete thread handles
            //
            for (int thread = 0; thread < nt; thread++)
                CloseHandle(threadH[thread]);

        }

    }

    cout << "finished" << endl << endl;

    //
    // output results so they can easily be pasted into a spredsheet from console window
    //
    for (int i = 0; i < indx; i++)
        cout << r[i] << endl;
    cout << endl;
    for (int i = 0; i < indx; i++)
        cout << diff[i] << endl;

    //
    // stop DOS window disappearing prematurely
    //
    _getch();

    return 0;

}

// eof